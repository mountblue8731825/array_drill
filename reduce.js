const items = [1, 2, 3, 4, 5, 5];

function reduce(elements, cb, startingValue) {
  var idx = 0;
  if (!startingValue) {
    startingValue = elements[0];
    idx = 1;
  }

  for (; idx < items.length; idx++) {
    startingValue = cb(startingValue, elements[idx]);
  }
  return startingValue;
}

function cb(startingValue, element) {
  return startingValue + element;
}

module.exports = { reduce, cb };
