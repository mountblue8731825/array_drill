const flatten = (elements) => {
  const res = [];

  console.log(flat(elements, res, 0));
};

function flat(elements, res, idx) {
  if (idx == elements.length) {
    return res;
  }

  if (typeof elements[idx] === "object") {
    flat(elements[idx], res, 0);
  } else {
    res.push(elements[idx]);
  }

  flat(elements, res, idx + 1);

  return res;
}

module.exports = flatten;
