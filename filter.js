function filter(elements, cb) {
  var res = [];
  for (var i = 0; i < elements.length; i++) {
    if (cb(elements[i], i)) {
      res.push(elements[i]);
    }
  }
  return res;
}

function cb(element, index) {
  if (element > 2) {
    return true;
  }
  return false;
}

module.exports = { filter, cb };
