const items = [1, 2, 3, 4, 5, 5];

function find(elements, cb) {
  for (var i = 0; i < elements.length; i++) {
    if (cb(elements[i], i)) {
      return true;
    }
  }

  return undefined;
}

function cb(element, index) {
  if (element > 2) {
    return true;
  }
  return false;
}

module.exports = { find, cb };
