const items = [1, 2, 3, 4, 5, 5];

const each = (elements, cb) => {
  for (var i = 0; i < items.length; i++) {
    cb(elements[i], i);
  }
};

function cb(element, index) {
  console.log(element, index);
}

module.exports = { each, cb };

each(items, cb);
