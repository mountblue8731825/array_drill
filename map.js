const map = (elements, cb) => {
  var result = [];
  for (var i = 0; i < elements.length; i++) {
    result.push(cb(elements[i], i));
  }
  return result;
};

function cb(element, id) {
  return element + 1;
}

module.exports = { map, cb };
